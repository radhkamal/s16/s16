console.log("Hello World");

let num = parseInt(prompt("Provide a number greater than 50: "));

console.log("The number you provided is " + num + ".");

for (i = num; i >= 50; i--) {

	if (i <= 50) {
		console.log("The current loop is at 50. Terminating the loop.")
		break;
	}

	if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if (i % 10 === 5 ) {
		console.log(i);
	}

}

let string = "supercalifragilisticexpialidocious";
let newString = "";

console.log(string);

for (let i=0; i < string.length; i++) {

	if (
		string[i] == "a" ||
		string[i] == "e" ||
		string[i] == "i" ||
		string[i] == "o" ||
		string[i] == "u" 
	) {
		continue;
	} else {

		if (i < string.length) {
			newString += string[i];
		}
	}
}

console.log(newString);

